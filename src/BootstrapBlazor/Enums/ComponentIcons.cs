﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

namespace BootstrapBlazor.Components;

/// <summary>
/// 组件内置图标枚举
/// </summary>
public enum ComponentIcons
{
    /// <summary>
    /// AnchorLink 组件 Icon 属性图标
    /// </summary>
    AnchorLinkIcon,

    /// <summary>
    /// Avatar 组件 Icon 属性图标
    /// </summary>
    AvatarIcon,

    /// <summary>
    /// AutoComplete 组件 Icon 属性图标
    /// </summary>
    AutoCompleteIcon,

    /// <summary>
    /// AutoComplete 组件 Icon 属性图标
    /// </summary>
    AutoFillIcon,

    /// <summary>
    /// Table 组件 SortIconAsc 属性图标
    /// </summary>
    TableSortIconAsc,

    /// <summary>
    /// Table 组件 SortDesc 属性图标
    /// </summary>
    TableSortDesc,

    /// <summary>
    /// Table 组件 SortIcon 属性图标
    /// </summary>
    TableSortIcon,

    /// <summary>
    /// Table 组件 FilterIcon 属性图标
    /// </summary>
    TableFilterIcon,

    /// <summary>
    /// Table 组件 ExportButtonIcon 属性图标
    /// </summary>
    TableExportButtonIcon
}
